﻿using System;
using System.Threading;
class Program{
static void Main(string[] args)
{

        //Order_Handler manejaOrdenes = new Order_Handler (); 
        Menu(); 

}

 static void Menu()
{
     Order_Handler manejaOrdenes = new Order_Handler();
    while (true)
    {
       
        Console.WriteLine("Seleccione una opción:");
        Console.WriteLine("1. Crear una orden");
        Console.WriteLine("2. Ver órdenes creadas");
        Console.WriteLine("3. Eliminar una orden");
        Console.WriteLine("4. Salir");

        int opcion = int.Parse(Console.ReadLine());
        int eliminar = 0;
        switch (opcion)
        {
            case 1:
                manejaOrdenes.create_order();
                manejaOrdenes.GuardarInfo("Archivo_ordenes.txt"); 
                break;
            case 2:
                
                manejaOrdenes.CargarInfoArchivo("Archivo_ordenes.txt"); 
                break;
            case 3:
                //  Console.WriteLine("Seleccione la orden a eliminar:");
                // eliminar = int.Parse(Console.ReadLine());
                manejaOrdenes.eliminar_orden();
                break;
            case 4:
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Opción inválida. Intente nuevamente.");
                break;
        }
    }
}
}