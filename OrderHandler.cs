﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

class Order_Handler
{

    List<int> lista_id = new List<int>();
    List<double> lista_precios = new List<double>();
    List<DateTime> lista_fechas_creacion = new List<DateTime>();
    List<string> lista_nombre_articulos = new List<string> { };
   

    public void create_order()
    {
        try
        {
            Console.WriteLine("Ingrese el ID de la orden:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el nombre del artículo:");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el precio del artículo:");
            double precio = double.Parse(Console.ReadLine());
            DateTime fechaCreacion = DateTime.Now;

            // Agregar elementos a las listas
            lista_id.Add(id);
            lista_nombre_articulos.Add(nombre);
            lista_precios.Add(precio);
            lista_fechas_creacion.Add(fechaCreacion);

            Console.WriteLine("Orden creada con éxito.");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }



    public void ver_ordenes()
    {
        if (lista_id.Count == 0)
        {
            Console.WriteLine("No hay órdenes creadas.");
        }
        else
        {
            Console.WriteLine("ID\tNombre\t\tPrecio\tFecha de creación");
            for (int i = 0; i < lista_id.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t\t{2}\t{3}", lista_id[i], lista_nombre_articulos[i], lista_precios[i], lista_fechas_creacion[i]);
            }
        }
    }

   public void eliminar_orden()
{
    try
    {
        if (lista_id.Count == 0)
        {
            Console.WriteLine("No hay ordenes creadas");
        }
        else
        {
            Console.WriteLine("Ingrese el ID de la orden que desea eliminar:");
            int idEliminar = int.Parse(Console.ReadLine());
            int index = lista_id.IndexOf(idEliminar);
            if (index <= -1)
            {
                Console.WriteLine("La orden no existe.");
            }
            else
            {
                lista_id.RemoveAt(index);
                lista_nombre_articulos.RemoveAt(index);
                lista_precios.RemoveAt(index);
                lista_fechas_creacion.RemoveAt(index);
                Console.WriteLine("Orden eliminada con éxito.");

                // Eliminar la orden del archivo .txt
                string archivoPath = "Archivo_ordenes.txt"; // Reemplaza "ruta_del_archivo.txt" con la ruta correcta
                string[] lineasArchivo = File.ReadAllLines(archivoPath);
                lineasArchivo = lineasArchivo.Where(linea => !linea.Contains(idEliminar.ToString())).ToArray();
                File.WriteAllLines(archivoPath, lineasArchivo);
                Console.WriteLine("Orden eliminada del archivo .txt.");
            }
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine("Error al eliminar la orden: " + ex.Message);
    }
}



    public void EliminarOrdenArchivo(string filePath, int idOrden)
{

    // string filePath = @"C:\Users\Dereck\OneDrive - ULACIT ED CR\Desktop\trabajosClase\csharp\tarea5/temp.txt";
    try
    {
        if (File.Exists(filePath))
        {
            string[] lines = File.ReadAllLines(filePath);
            bool ordenEncontrada = false;

            List<string> lineasActualizadas = new List<string>();

            foreach (string line in lines)
            {
                string[] orderData = line.Split(',');

                if (orderData.Length == 4)
                {
                    int id = int.Parse(orderData[0]);

                    if (id == idOrden)
                    {
                        ordenEncontrada = true;
                        continue; // Omite la línea correspondiente a la orden eliminada
                    }
                }

                lineasActualizadas.Add(line);
            }

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                foreach (string lineaActualizada in lineasActualizadas)
                {
                    writer.WriteLine(lineaActualizada);
                }
            }

            if (ordenEncontrada)
            {
                Console.WriteLine("Orden eliminada del archivo con éxito.");
            }
            else
            {
                Console.WriteLine("La orden no existe en el archivo.");
            }
        }
        else
        {
            Console.WriteLine("El archivo no existe.");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine("Error al eliminar la orden del archivo: " + ex.Message);
    }
}
   public void GuardarInfo(string filePath)
{
    try
    {
        if (File.Exists(filePath))
        {
            List<string> existingOrders = File.ReadAllLines(filePath).ToList();

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                for (int i = 0; i < lista_id.Count; i++)
                {
                    string orderData = $"{lista_id[i]},{lista_nombre_articulos[i]},{lista_precios[i]},{lista_fechas_creacion[i]}";
                    if (!existingOrders.Contains(orderData))
                    {
                        writer.WriteLine(orderData);
                    }
                }
            }
        }
        else
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                for (int i = 0; i < lista_id.Count; i++)
                {
                    writer.WriteLine("{0},{1},{2},{3}", lista_id[i], lista_nombre_articulos[i], lista_precios[i], lista_fechas_creacion[i]);
                }
            }
        }

        Console.WriteLine("Órdenes guardadas en el archivo con éxito.");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Error al guardar las órdenes en el archivo: " + ex.Message);
    }
}

    public void CargarInfoArchivo(string filePath) /// Carga de informacion desde archivo txt
    {
        try
        {
            if (File.Exists(filePath))
            {
                lista_id.Clear();
                lista_nombre_articulos.Clear();
                lista_precios.Clear();
                lista_fechas_creacion.Clear();

                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] orderData = line.Split(',');

                        if (orderData.Length == 4)
                        {
                            int id = int.Parse(orderData[0]);
                            string nombre = orderData[1];
                            double precio = double.Parse(orderData[2]);
                            DateTime fechaCreacion = DateTime.Parse(orderData[3]);

                            lista_id.Add(id);
                            lista_nombre_articulos.Add(nombre);
                            lista_precios.Add(precio);
                            lista_fechas_creacion.Add(fechaCreacion);
                        }
                    }
                }
                 Console.WriteLine("ID\tNombre\t\tPrecio\tFecha de creación");
            for (int i = 0; i < lista_id.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t\t{2}\t{3}", lista_id[i], lista_nombre_articulos[i], lista_precios[i], lista_fechas_creacion[i]);
            }
                Console.WriteLine("Órdenes cargadas desde el archivo con éxito.");
            }
            else
            {
                Console.WriteLine("El archivo no existe.");  /// Manejo de excepciones
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error al cargar las órdenes desde el archivo: " + ex.Message);
        }
    }

}




